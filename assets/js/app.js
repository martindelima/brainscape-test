// Apply menu item text
var submenuItems = document.getElementsByClassName("js-submenu-text"),
    heading = document.querySelector("#hero-heading");

var applyText = function() {
  var itemText = this.textContent;
  heading.textContent = this.textContent;
};

for (let i = 0; i < submenuItems.length; i++) {
  submenuItems[i].addEventListener("click", applyText, false);
}
